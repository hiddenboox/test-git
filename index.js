const gitLogParser = require("git-log-parser");
const getStream = require("get-stream");
const execa = require("execa");
const { relative, resolve, join} = require("path");

const main = async ({ firstParentBranch = undefined, lastHead = undefined } = {}) => {
const root = (await execa("git", ["rev-parse", "--show-toplevel"], { cwd: process.cwd() })).stdout;

Object.assign(gitLogParser.fields, {
    hash: "H",
    message: "B",
    gitTags: "d",
    committerDate: { key: "ci", type: Date },
});

const relpath = relative(root, join(root, 'packages/vendors'));
const firstParentBranchFilter = firstParentBranch ? ["--first-parent", firstParentBranch] : [];
const gitLogFilterQuery = [...firstParentBranchFilter, lastHead ? `${lastHead}..HEAD` : "HEAD", "--", relpath];
const stream = gitLogParser.parse({ _: gitLogFilterQuery }, { cwd: process.cwd(), env: process.env });
const commits = await getStream.array(stream);

// Trim message and tags.
commits.forEach((commit) => {
    console.log(commit.subject)
});
}

main({ lastHead: 'master' });
// main();
