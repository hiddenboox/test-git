module.exports = {
  branches: ["master", { name: "next", prerelease: true, channel: "next" }],
  plugins: [
    "@semantic-release/commit-analyzer",
    "@semantic-release/release-notes-generator",

    [
      "@semantic-release/changelog",
      {
        changelogFile: "CHANGELOG.md",
      },
    ],
    "@semantic-release/npm",
    [
      "@semantic-release/git",
      {
        assets: ["CHANGELOG.md", "package.json", "package-lock.json"],
        message: "chore(release): ${nextRelease.version} [skip ci]",
      },
    ],
    [
      "@saithodev/semantic-release-backmerge",
      {
        branchName: "next"
      }
    ],
  ].filter(Boolean),
  noCi: true,
};
