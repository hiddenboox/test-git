# [1.17.0](https://gitlab.com/hiddenboox/test-git/compare/v1.16.0...v1.17.0) (2021-04-28)


### Features

* xca22x2 ([043f1e5](https://gitlab.com/hiddenboox/test-git/commit/043f1e5e9dcacd970ac7f90506bb66231c8d09f3))
* xca22x22 ([81f94c8](https://gitlab.com/hiddenboox/test-git/commit/81f94c825ed146d7045b292ca5c959b67cf57513))

# [1.16.0](https://gitlab.com/hiddenboox/test-git/compare/v1.15.0...v1.16.0) (2021-04-28)


### Features

* xca22x ([fd10de6](https://gitlab.com/hiddenboox/test-git/commit/fd10de69912940defffe61757b1aa7b90c90f17f))

# [1.15.0](https://gitlab.com/hiddenboox/test-git/compare/v1.14.0...v1.15.0) (2021-04-28)


### Features

* xca22 ([2b23e97](https://gitlab.com/hiddenboox/test-git/commit/2b23e97c90891a8e4069df078ccc4196c110ee90))

# [1.14.0](https://gitlab.com/hiddenboox/test-git/compare/v1.13.0...v1.14.0) (2021-04-28)


### Features

* xca ([6f809df](https://gitlab.com/hiddenboox/test-git/commit/6f809dfbf568dbc33686601983b98615adf3a8c5))

# [1.13.0](https://gitlab.com/hiddenboox/test-git/compare/v1.12.0...v1.13.0) (2021-04-28)


### Features

* xw1 ([264b741](https://gitlab.com/hiddenboox/test-git/commit/264b7410c12102584ca4b8d68070d765f2361989))

# [1.12.0](https://gitlab.com/hiddenboox/test-git/compare/v1.11.0...v1.12.0) (2021-04-28)


### Features

* xa ([3f5d02a](https://gitlab.com/hiddenboox/test-git/commit/3f5d02ae5ad36e8682de2fe66a7bfb07c15ed8bf))

# [1.11.0](https://gitlab.com/hiddenboox/test-git/compare/v1.10.0...v1.11.0) (2021-04-28)


### Features

* ty ([45c1b04](https://gitlab.com/hiddenboox/test-git/commit/45c1b048c63e8890049cf561ab9e0aea87bbfb78))

# [1.10.0](https://gitlab.com/hiddenboox/test-git/compare/v1.9.0...v1.10.0) (2021-04-28)


### Features

* xx ([efd050c](https://gitlab.com/hiddenboox/test-git/commit/efd050c3a4bb8cb70eb6f8f4e41086bba10ae699))

# [1.9.0](https://gitlab.com/hiddenboox/test-git/compare/v1.8.0...v1.9.0) (2021-04-27)


### Features

* aaa ([b2ccf06](https://gitlab.com/hiddenboox/test-git/commit/b2ccf0656f8b2514cf2669f011fa307205bd5d28))

# [1.8.0](https://gitlab.com/hiddenboox/test-git/compare/v1.7.0...v1.8.0) (2021-04-27)


### Features

* wwww ([83cea53](https://gitlab.com/hiddenboox/test-git/commit/83cea537e128c2d9c4e7d65ec4f55f58f09a26d9))

# [1.7.0](https://gitlab.com/hiddenboox/test-git/compare/v1.6.0...v1.7.0) (2021-04-27)


### Features

* tellow ([8c670c6](https://gitlab.com/hiddenboox/test-git/commit/8c670c6ad42efb1d81e4cac64933a880d6b45c77))

# [1.6.0](https://gitlab.com/hiddenboox/test-git/compare/v1.5.0...v1.6.0) (2021-04-27)


### Features

* next1xy ([704fcd2](https://gitlab.com/hiddenboox/test-git/commit/704fcd2a4a3fa7376e05e02a3dc9283df2a291d0))

# [1.5.0](https://gitlab.com/hiddenboox/test-git/compare/v1.4.0...v1.5.0) (2021-04-27)


### Features

* next1x ([f640d6a](https://gitlab.com/hiddenboox/test-git/commit/f640d6a83ee8ebb994c1f31c9b56fcf20d891cec))

# [1.4.0](https://gitlab.com/hiddenboox/test-git/compare/v1.3.0...v1.4.0) (2021-04-27)


### Features

* next1 ([fe39442](https://gitlab.com/hiddenboox/test-git/commit/fe394425521abe98483a8fad76c23b4ef1d6d7f5))

# [1.3.0](https://gitlab.com/hiddenboox/test-git/compare/v1.2.5...v1.3.0) (2021-04-27)


### Features

* next ([99c4c44](https://gitlab.com/hiddenboox/test-git/commit/99c4c44b1af0f80e31071d17ab7c3fed2c7c40a4))

## [1.2.5](https://gitlab.com/hiddenboox/test-git/compare/v1.2.4...v1.2.5) (2021-04-27)


### Bug Fixes

* publish ([a17b997](https://gitlab.com/hiddenboox/test-git/commit/a17b9971eb85b586bf9f2da13b9d8dc93814eca3))

## [1.2.4](https://gitlab.com/hiddenboox/test-git/compare/v1.2.3...v1.2.4) (2021-04-27)


### Bug Fixes

* publish ([c825cae](https://gitlab.com/hiddenboox/test-git/commit/c825cae4bbad5f67c503b3d2c05214006b488f77))
* publish ([9e2eea0](https://gitlab.com/hiddenboox/test-git/commit/9e2eea02972748afb3d34e2c595e59f9d9832f46))

## [1.2.3](https://gitlab.com/hiddenboox/test-git/compare/v1.2.2...v1.2.3) (2021-04-27)


### Bug Fixes

* publish ([ff18385](https://gitlab.com/hiddenboox/test-git/commit/ff1838531a1567d4f104006266beda05fa7a82d0))

## [1.2.2](https://gitlab.com/hiddenboox/test-git/compare/v1.2.1...v1.2.2) (2021-04-27)


### Bug Fixes

* publish ([8c0d34e](https://gitlab.com/hiddenboox/test-git/commit/8c0d34e631811e2de4ac037f14425edbfa10ebdd))
* publish ([d28b002](https://gitlab.com/hiddenboox/test-git/commit/d28b0029a410978ec7a1e4f23245c01af4e98659))
* publish ([b1e1fce](https://gitlab.com/hiddenboox/test-git/commit/b1e1fcec2e7b878861729a8203dd1a861b2adc6a))

## [1.2.1](https://gitlab.com/hiddenboox/test-git/compare/v1.2.0...v1.2.1) (2021-04-27)


### Bug Fixes

* aa ([ea0e450](https://gitlab.com/hiddenboox/test-git/commit/ea0e4500a3a67c674598b79076b375715c911560))

# [1.2.0](https://gitlab.com/hiddenboox/test-git/compare/v1.1.0...v1.2.0) (2021-04-27)


### Features

* **gow:** new221c ([4a6807d](https://gitlab.com/hiddenboox/test-git/commit/4a6807ddce892aaf5d81c336babdc68d6f23060d))

# [1.2.0-next.1](https://gitlab.com/hiddenboox/test-git/compare/v1.1.0...v1.2.0-next.1) (2021-04-27)


### Features

* **gow:** new221c ([4a6807d](https://gitlab.com/hiddenboox/test-git/commit/4a6807ddce892aaf5d81c336babdc68d6f23060d))

# [1.1.0](https://gitlab.com/hiddenboox/test-git/compare/v1.0.0...v1.1.0) (2021-04-27)


### Features

* **gow:** new cc ([9d61c45](https://gitlab.com/hiddenboox/test-git/commit/9d61c452170065f35b0160f26baf050c3c29b15f))

# [1.1.0-next.1](https://gitlab.com/hiddenboox/test-git/compare/v1.0.0...v1.1.0-next.1) (2021-04-27)


### Features

* **gow:** new cc ([9d61c45](https://gitlab.com/hiddenboox/test-git/commit/9d61c452170065f35b0160f26baf050c3c29b15f))

# 1.0.0 (2021-04-27)


### Features

* **ci:** release config ([9024900](https://gitlab.com/hiddenboox/test-git/commit/902490064b217ad4f0bad454fffe1c10e99bd3de))
* **gow:** new f2 ([972885e](https://gitlab.com/hiddenboox/test-git/commit/972885ef64765aed153a7625a9ac450e0a62664a))
* **gow:** new fi ([4d61164](https://gitlab.com/hiddenboox/test-git/commit/4d61164c0724d7b339601e0f7a2e87121ce26aa7))
* **gow:** new fi ([a18f9da](https://gitlab.com/hiddenboox/test-git/commit/a18f9daa46709624982c34b6fd4b7fec1ad2fa01))
* **vendors:** add ([9b0365c](https://gitlab.com/hiddenboox/test-git/commit/9b0365ce218fe53e530ec163bd48a0997301c153))
* **vendors:** add 2 ([12913d3](https://gitlab.com/hiddenboox/test-git/commit/12913d3f7ec69a742f55dd9e06608996b5e58b7b))
* **vendors:** add 3 ([2ef8f92](https://gitlab.com/hiddenboox/test-git/commit/2ef8f92ffc705e21d56cce1905b171a342abfb08))
* **vendors:** add 3 ([4ce66f2](https://gitlab.com/hiddenboox/test-git/commit/4ce66f26c5d12be25f0dd291d7cd7bddc7e5aa82))

# [1.1.0](https://gitlab.com/hiddenboox/test-git/compare/v1.0.0...v1.1.0) (2021-04-27)


### Features

* **gow:** new fi ([4d61164](https://gitlab.com/hiddenboox/test-git/commit/4d61164c0724d7b339601e0f7a2e87121ce26aa7))

# 1.0.0 (2021-04-27)


### Features

* **ci:** release config ([9024900](https://gitlab.com/hiddenboox/test-git/commit/902490064b217ad4f0bad454fffe1c10e99bd3de))
* **gow:** new fi ([a18f9da](https://gitlab.com/hiddenboox/test-git/commit/a18f9daa46709624982c34b6fd4b7fec1ad2fa01))
* **vendors:** add ([9b0365c](https://gitlab.com/hiddenboox/test-git/commit/9b0365ce218fe53e530ec163bd48a0997301c153))
* **vendors:** add 2 ([12913d3](https://gitlab.com/hiddenboox/test-git/commit/12913d3f7ec69a742f55dd9e06608996b5e58b7b))
* **vendors:** add 3 ([2ef8f92](https://gitlab.com/hiddenboox/test-git/commit/2ef8f92ffc705e21d56cce1905b171a342abfb08))
* **vendors:** add 3 ([4ce66f2](https://gitlab.com/hiddenboox/test-git/commit/4ce66f26c5d12be25f0dd291d7cd7bddc7e5aa82))

# [1.4.0](https://gitlab.com/hiddenboox/test-git/compare/v1.3.0...v1.4.0) (2021-04-27)


### Features

* **gow:** new fi ([a18f9da](https://gitlab.com/hiddenboox/test-git/commit/a18f9daa46709624982c34b6fd4b7fec1ad2fa01))

# [1.3.0](https://gitlab.com/hiddenboox/test-git/compare/v1.2.0...v1.3.0) (2021-04-27)


### Features

* **ci:** release config ([9024900](https://gitlab.com/hiddenboox/test-git/commit/902490064b217ad4f0bad454fffe1c10e99bd3de))

# [1.2.0](https://gitlab.com/hiddenboox/test-git/compare/v1.1.0...v1.2.0) (2021-04-27)


### Features

* **vendors:** add 3 ([2ef8f92](https://gitlab.com/hiddenboox/test-git/commit/2ef8f92ffc705e21d56cce1905b171a342abfb08))
